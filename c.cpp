#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
using namespace std;

vector<vector<int>> matmul(vector<vector<int>> mat1, vector<vector<int>> mat2)
{
    int r1 = mat1.size();
    int c1 = mat1[0].size();
    int r2 = mat2.size();
    int c2 = mat2[0].size();
    if(c1 != r2)
        cout << "Can't multiply. Wrong matrix dimensions" << endl;
    vector<vector<int>> res;
    for(int i = 0; i < r1; i++)
    {
        vector<int> temp(c2, 0);
        res.push_back(temp);
    }

    for(int i = 0; i < r1; ++i)
     {   
         for(int j = 0; j < c2; ++j)
         {   
             for(int k = 0; k < c1; ++k)
             {
                res[i][j] += mat1[i][k] * mat2[k][j];
             }
         }
     }

    return res;
}

int main(int argc, char * argv[])
{

ifstream inputfile;
inputfile.open(argv[1]);
ofstream outputfile;
outputfile.open("output.txt");

string line;
int index = 1;
vector<vector<int>> matrix1;
vector<vector<int>> matrix2;
while(getline(inputfile, line))
{
    istringstream iss(line);
    if(iss.length()==0)
        {
            if(index ==1)
            {index = 2; continue;}
            else
            {
                index = 3;
            }
            
        }
    
    if(index ==1)
    {
        vector<int> temp;
        int n;
        while(iss >> n)
        {
            temp.push_back(n);
        }
        matrix1.push_back(temp);
    }
    else if(index = 2)
    {
        vector<int> temp;
        int n;
        while(iss >> n)
        {
            temp.push_back(n);
        }
        matrix2.push_back(temp);
    }
    else
    {
        vector<vector<int>> res = matmul(matrix1, matrix2);
        int r = res.size();
        int c = res[0].size();
        for(int i = 0; i < r; ++i)
        {
            for(int j = 0; j < c; ++j)
            {
                outputfile << res[i][j] << ' ';
                if(j == c-1)
                    outputfile << '\n';
            } 
        }

        index = 1;
    }
    
    
}
/*
vector<vector<int>> res = matmul(matrix1, matrix2);
int r = res.size();
int c = res[0].size();
 for(int i = 0; i < r; ++i)
    for(int j = 0; j < c; ++j)
    {
        cout << " " << res[i][j];
        if(j == c-1)
            cout << endl;
    }
*/
inputfile.close();
outputfile.close();
return 0;
}